package com.placheta.sortingalgorithms;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void bubbleClicked(View view){
        createDetailedActivity("bubble",view);
    }

    public void qsClicked(View view){
        createDetailedActivity("qs",view);
    }

    public void shellClicked(View view){
        createDetailedActivity("shell",view);
    }

    public void selectClicked(View view){ createDetailedActivity("select",view); }

    public void insertClicked(View view){
        createDetailedActivity("insert",view);
    }

    private void createDetailedActivity(String actName, View view){
        Intent act2 = new Intent(view.getContext(),DetailsActivity.class);
        act2.putExtra("name",actName);
        startActivity(act2);
    }

}
