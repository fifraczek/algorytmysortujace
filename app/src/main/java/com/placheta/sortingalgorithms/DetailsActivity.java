package com.placheta.sortingalgorithms;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import org.w3c.dom.Text;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent i = getIntent();
        String algorithmName = i.getStringExtra("name");

        TextView titleView = (TextView) findViewById(R.id.titleTextView);
        TextView contentView = (TextView) findViewById(R.id.contentTextView);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);



        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        float d = this.getResources().getDisplayMetrics().density;

        switch (algorithmName){
            case "bubble":
            titleView.setText(R.string.bubble_title);
            contentView.setText(R.string.bubble_content);
                imageView.setImageResource(R.drawable.bubble_sort);
                lp.setMargins(0, (int)d*(-80), 0, (int)d*(-80));
            break;

            case "insert":
                titleView.setText(R.string.insert_title);
                contentView.setText(R.string.insert_content);
                imageView.setImageResource(R.drawable.insert_sort);
                lp.setMargins(0, (int)d*(-170), 0, (int)d*(-170));
            break;

            case "shell":
                titleView.setText(R.string.shell_title);
                contentView.setText(R.string.shell_content);
                imageView.setImageResource(R.drawable.shell_sort);
                lp.setMargins(0, (int)d*(-240), 0, (int)d*(-240));
            break;

            case "qs":
                titleView.setText(R.string.qs_title);
                contentView.setText(R.string.qs_content);
                imageView.setImageResource(R.drawable.qs_sort);
                lp.setMargins(0, (int)d*(-240), 0, (int)d*(-240));
            break;

            case "select":
                titleView.setText(R.string.select_title);
                contentView.setText(R.string.select_content);
                imageView.setImageResource(R.drawable.select_sort);
            break;

        default:
        break;
        }

        imageView.setLayoutParams(lp);
    }
}
